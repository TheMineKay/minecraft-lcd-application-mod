package ricksoft.minecraft.lcd;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class LCDListener extends Thread
{
  public Boolean running = true;
  ServerSocket serverSocket;
  LCDConnectionListener callBk;

  public void stopListening()
  {
    if (this.running != null) 
    	this.running = false; 
  }

  public LCDListener(ServerSocket svrSock, LCDConnectionListener callBack)
  {
    this.serverSocket = svrSock;
    this.callBk = callBack;
  }

  public void run()
  {
    while (this.running)
    {
      try
      {
        System.out.println("Listening started.");
        this.callBk.sock = this.serverSocket.accept();
        this.callBk.out = new PrintWriter(this.callBk.sock.getOutputStream(), true);
        System.out.println("Client accepted.");

        this.callBk.playerListener.Reset();
      }
      catch (Exception e)
      {
        if (this.running) System.out.println("Error while accepting client on localhost:" + LCDConnectionListener.port + " -> " + e.getMessage() + e.getStackTrace());
      }
    }
  }
}