package ricksoft.minecraft.lcd;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import net.minecraft.client.Minecraft;

public class LCDConnectionListener
{
  public static int port = 12583;
  Minecraft minecraftBase;
  ServerSocket serverSock;
  public Socket sock;
  Boolean running = Boolean.valueOf(true);
  public PrintWriter out;
  public LCDPlayerStateListener playerListener;
  public LCDListener listenThread;

  public void shutdown()
  {
    this.listenThread.stopListening();
    try {
      this.serverSock.close();
    }
    catch (IOException e)
    {
    }
  }

  public void Update(String command)
  {
    if (this.out == null) 
    	return;
    
    if (this.sock == null) 
    	return;

    try
    {
      this.out.println(command);
    } catch (Exception e) {
      System.out.println("Error, lost connection to client on localhost:" + port + " -> " + e.getMessage() + "/n" + e.getStackTrace());

      this.out = null;
      this.sock = null;
    }
  }

  public LCDConnectionListener(Minecraft mc) throws IOException
  {
    this.minecraftBase = mc;
    this.serverSock = new ServerSocket(port);
    this.listenThread = new LCDListener(this.serverSock, this);
    this.listenThread.start();

    this.playerListener = new LCDPlayerStateListener(mc, this);
    this.playerListener.start();
  }
}